# Тестовое задание Sibers #

 [Задание] (http://hr.sibers.com/PHP-test-1.pdf)

### Процесс установки ###

1. Клонировать проект - git clone git@bitbucket.org:noolkmvm/sibers.git
2. Установить библиотеку для автозагрузки классов - composer install
3. Перейти в app/config в файле Database.app выполнить поключение к свой бд.
4. Импортировать таблицу users.

### Приложение установлено ###

Логин: admin
Пароль: 123