<?php

namespace App;

use App\controllers\adminController;
use App\controllers\authController;
use App\controllers\mainController;

class Router{
    public static $routes = [
        '~^$~' => [mainController::class, 'action_index'],
        '~^login/$~' => [authController::class, 'action_login'],
        '~^auth/$~' => [authController::class, 'action_auth'],
        '~^logout/$~' => [authController::class, 'action_logout'],
        '~^add/$~' => [adminController::class, 'action_adduser'],
        '~^editing/$~' => [adminController::class, 'action_editUserProfile'],
        '~^remove/$~' => [adminController::class, 'action_remove'],
    ];

}