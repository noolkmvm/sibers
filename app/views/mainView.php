<?php
if(!isset($_SESSION['auth'])) {
    echo
    '<script type="text/javascript">
        location.replace("/login/");
    </script>';
} ?>

<?php if($_SESSION['staff_status'] == '1'): ?>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Sibers</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
                </ul>
                    <a type="button" class="btn btn-success" href="/logout/">Выйти</a>
            </div>
    </nav>

    <div class="p-3">
        <div type="button" class="btn btn-success" data-toggle="modal" data-target="#userAddModal">Добавить пользователя</div>
        <div class="modal fade" id="userAddModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Добавить нового пользователя</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/add/">
                            <input type="text" name="login" class="form-control" placeholder="Логин" required>
                            <input type="password" name="password" class="form-control" placeholder="Пароль" required>
                            <input type="text" name="first_name" class="form-control" placeholder="Имя" required>
                            <input type="text" name="last_name" class="form-control" placeholder="Фамилия" required>
                            <select class="form-select" name="gender" required>
                                <option selected>Пол</option>
                                <option value="0">Мужской</option>
                                <option value="1">Женский</option>
                            </select>
                            <input type="date" name="date" class="form-control" required>
                            <hr>
                            <button class="btn btn-success btn-round" type="submit">Добавить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col" class="col-md-1"><a href="/?page=<?php echo !isset($_GET['page']) ? 1 : $_GET['page']; ?>&sort=<?php echo !isset($_GET['sort']) || $_GET['sort'] == '2' ? 1 : 2; ?>" style="color: black"># <i class="fa fa-sort" aria-hidden="true"></i></a></th>
            <th scope="col" class="col-md-3">Логин</th>
            <th scope="col" class="col-md-3">Имя</th>
            <th scope="col" class="col-md-3">Дествия</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($data['users'] as $row): ?>
            <tr>
                <th scope="row"><?php echo $row['id']; ?></th>
                <td><?php echo $row['login']; ?></td>
                <td><?php echo $row['first_name']; ?></td>
                <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#viewingModal-<?php echo $row['id']; ?>">Информация</button>
                <div class="modal fade" id="viewingModal-<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Информация о пользователе</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="card">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Имя: <b><?php echo $row['first_name']; ?></b></li>
                                        <li class="list-group-item">Фамилия: <b><?php echo $row['last_name']; ?></b></li>
                                        <li class="list-group-item">Логин: <b><?php echo $row['login']; ?></b></li>
                                        <li class="list-group-item">Пол: <b><?php if($row['gender'] == '0'){echo 'Мужской';}else echo 'Женский'; ?></b></li>
                                        <li class="list-group-item">Дата рождения: <b><?php echo $row['date']; ?></b></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#userEditModal-<?php echo $row['id']; ?>">Редактировать</button>
                <div class="modal fade" id="userEditModal-<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Редактировать данные пользователя</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="/editing/" id="task=<?php echo $row['id']; ?>">
                                    <input class="form-check-input" type="text" name="id" value="<?php echo $row['id']; ?>" hidden="true">
                                    <input type="text" name="login" class="form-control" value="<?php echo $row['login']; ?>" required>
                                    <input type="password" name="password" class="form-control" value="<?php echo $row['password']; ?>" required>
                                    <input type="text" name="first_name" class="form-control" value="<?php echo $row['first_name']; ?>" required>
                                    <input type="text" name="last_name" class="form-control" value="<?php echo $row['last_name']; ?>" required>
                                    <select class="form-select" name="gender" required>
                                        <?php if($row['gender'] == '0'): ?>
                                        <option value="0" selected>Мужской</option>
                                        <option value="1" >Женский</option>
                                        <?php else: ?>
                                            <option value="1" selected>Женский</option>
                                            <option value="0" >Мужской</option>
                                        <?php endif; ?>
                                    </select>
                                    <input type="date" name="date" class="form-control" value="<?php echo $row['date']; ?>" required>
                                    <hr>
                                    <button class="btn btn-success btn-round" type="submit">Редактировать</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-danger"><a href="/remove/?id=<?php echo $row['id']; ?>">Удалить</a></button></th>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        <?php
        for($i=1; $i<= $data['countPages']; $i++) {
            echo "<li class=\"page-item\"><a class=\"page-link\" href=\"\?page={$i}\" style=\"color: black;\">{$i}</a></li>";
        }
        ?>
    </ul>
</nav>

<?php else: ?>
<div class="container">
    <h1>Привет, <?php echo $_SESSION['login']; ?></h1>
    <p>Тут должен быть web-интерфейс БД зарегистрированных пользователей сайта, но у тебя нет прав администратора :(</p>
    <a type="button" class="btn btn-success" href="/logout/">Выйти</a>
</div>

<?php endif; ?>
