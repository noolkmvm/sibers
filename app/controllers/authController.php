<?php


namespace App\controllers;


use App\core\Controller;
use App\core\View;
use App\models\authModel;

class authController extends Controller{

    function __construct(){
        $this->model = new authModel();
        $this->view = new View();
    }

    function action_login(){
        $this->view->generate('authView.php', 'template.php');
    }

    function action_auth(){
        $this->model->auth();
        header("Location: /");
    }

    public function action_logout(){
        session_destroy();
        header("Location: /login/");
    }
}