<?php

namespace App\controllers;

use App\core\Controller;
use App\models\adminModel;

class adminController extends Controller{
    function __construct(){
        $this->model = new adminModel();
    }

    function action_adduser(){
        $this->model->addUser();
        header("Location: /");
    }

    function action_editUserProfile(){
        $this->model->editUserProfile();
        header("Location: /");
    }

    function action_remove(){
        $this->model->removeUser();
        header("Location: /");
    }
}