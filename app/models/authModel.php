<?php

namespace App\models;

use App\core\Model;

class authModel extends Model{
    public function auth() {
        $login = $_POST['login'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM users WHERE login = :login AND password = :password";

        $state = $this->db->prepare($sql);
        $state->bindParam(":login", $login);
        $state->bindParam(":password", $password);
        $state->execute();

        $user = $state->fetch(\PDO::FETCH_ASSOC);

        if(!empty($user)){
            $_SESSION['auth'] = true;
            $_SESSION['login'] = $_POST['login'];
            $_SESSION['staff_status'] = $user['staff_status'];
        }else {
            $errors = 'Неверное имя пользователя или пароль';
            return $errors;
        }
    }

}