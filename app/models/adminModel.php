<?php

namespace App\models;

use App\core\Model;

class adminModel extends Model{
    public function checkLogin($login){
        $sql  = 'SELECT * FROM users WHERE login = :login';

        $state = $this->db->prepare($sql);
        $state->bindParam(":login", $login);
        $state->execute();

        if(!empty($state->fetch(\PDO::FETCH_ASSOC))){
            return true;
        }
    }
    public function addUser() {
        $login = $_POST['login'];
        $password = $_POST['password'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $gender = $_POST['gender'];
        $date = $_POST['date'];
        $staff_status = 0;

        $uniq = $this->checkLogin($login);

        if(!isset($uniq)){
            $sql = 'INSERT INTO users (login,password,first_name,last_name,gender,date, staff_status) VALUES (:login,:password,:first_name,:last_name,:gender,:date, :staff_status);';

            $state = $this->db->prepare($sql);
            $state->bindParam(":login", $login);
            $state->bindParam(":password", $password);
            $state->bindParam(":first_name", $first_name);
            $state->bindParam(":last_name", $last_name);
            $state->bindParam(":gender", $gender);
            $state->bindParam(":date", $date);
            $state->bindParam(":staff_status", $staff_status);
            $state->execute();
        }else{
            $error = 'Пользователь с такми логином уже существует!';
            return $error;
        }


    }
    public function editUserProfile() {
        $id = $_POST['id'];
        $login = $_POST['login'];
        $password = $_POST['password'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $gender = $_POST['gender'];
        $date = $_POST['date'];
        $staff_status = 0;

        $sql = "UPDATE users SET login = ?, password = ?, first_name = ?, last_name = ?, gender = ?, date = ?, staff_status = ? WHERE id = ?";

        $state = $this->db->prepare($sql);
        $state->bindParam(1, $login);
        $state->bindParam(2, $password);
        $state->bindParam(3, $first_name);
        $state->bindParam(4, $last_name);
        $state->bindParam(5, $gender);
        $state->bindParam(6, $date);
        $state->bindParam(7, $staff_status);
        $state->bindParam(8, $id);
        $state->execute();
    }

    public function removeUser() {
        $id = $_GET['id'];
        $sql = 'DELETE FROM users WHERE id = :id';
        $state = $this->db->prepare($sql);
        $state->bindParam(":id", $id);
        $state->execute();
    }
}